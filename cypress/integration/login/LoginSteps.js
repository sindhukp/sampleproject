import {Given , When, Then} from "cypress-cucumber-preprocessor/steps";

Given('Login to the app', () => {
    cy.visit('http://demo.t3-framework.org/joomla30/index.php/en/joomla-pages/sample-page-2/login-page')

})

When('Enter valid cred', () => {

    cy.get('#username').type('test')
    cy.get('#password').type('test123')
    
})

Then('click Login', () => {
    cy.get('button[type="submit"]').click()
  })

  And('See the error', () => {
    cy.get('p:contains("Username and password do not match or you do not have an account yet.")').should('be.visible')
  })

